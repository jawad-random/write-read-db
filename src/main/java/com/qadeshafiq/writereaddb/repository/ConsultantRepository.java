package com.qadeshafiq.writereaddb.repository;

import com.qadeshafiq.writereaddb.modal.ConsultantEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface ConsultantRepository extends JpaRepository<ConsultantEntity, Long> {

    List<ConsultantEntity> findBySubject(String subject);

    List<ConsultantEntity> findBySubjectAndName(String subject, String name);

    @Transactional
    void deleteByName(String name);
}
