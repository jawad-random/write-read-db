package com.qadeshafiq.writereaddb.modal;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CONSULTANTS")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ConsultantEntity {

    @Id
    private long regNo;

    @Column(name = "Name")
    private String name;

    @Column(name = "Subject")
    private String subject;

    @Column(name = "Client")
    private String client;

    @Column(name = "Marks")
    private double marks;

}
